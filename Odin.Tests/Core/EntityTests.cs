﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Odin.ECS.Core;
using Odin.ECS.Core.Components;
using Odin.Samples.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odin.Tests.Core
{
  [TestClass]
  public class EntityTests
  {
    private EntityWorld _world;

    [TestInitialize]
    public void Init()
    {
      _world = new EntityWorld();
    }

    [TestCleanup]
    public void Cleanup()
    {
      _world.Cleanup();
    }

    [TestMethod]
    public void CreateEntity_Test()
    {
      var entity = _world.CreateEntity();
      Assert.IsNotNull(entity);
    }

    [TestMethod]
    public void CreateEntityFromArchetype_Test()
    {
      var archetype = _world.CreateArchetype(() => new PositionComponent());
      var entity = _world.CreateEntity(archetype);
      Assert.IsTrue(entity.Has<PositionComponent>());
    }

    [TestMethod]
    public void RemoveEntity_Test()
    {
      var entity = _world.CreateEntity();
      _world.RemoveEntity(entity);

      Assert.AreEqual(0, _world.GetEntities().Count());
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentNullException))]
    public void RemoveNullEntity_Test()
    {
      _world.RemoveEntity(null);
    }

  }
}
