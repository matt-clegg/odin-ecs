﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Odin.ECS.Core;
using Odin.ECS.Core.Components;
using Odin.Samples.Components;
using System;
using System.Collections.Generic;

namespace Odin.Tests.Core
{
  [TestClass]
  public class ComponentsTests
  {

    private EntityWorld _world;

    [TestInitialize]
    public void Init()
    {
      _world = new EntityWorld();
    }

    [TestMethod]
    public void AddMultipleComponentsFromParams_Test()
    {
      var entity = _world.CreateEntity();
      entity.Add(new PositionComponent(), new VelocityComponent(1, 2));

      Assert.IsTrue(entity.Has<PositionComponent>() && entity.Has<VelocityComponent>());
    }

    [TestMethod]
    public void AddMultipleComponentsFromList_Test()
    {
      var entity = _world.CreateEntity();

      var components = new List<IComponent>()
      {
        new PositionComponent(),
        new VelocityComponent(1, 2)
      };
      
      entity.Add(components);

      Assert.IsTrue(entity.Has<PositionComponent>() && entity.Has<VelocityComponent>());
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidOperationException))]
    public void EntityGetUnknownComponent_Test()
    {
      var entity = _world.CreateEntity();
      entity.Add(new PositionComponent());

      entity.Get<VelocityComponent>();
    }

    [TestMethod]
    public void EntityHasComponent_Test()
    {
      var entity = _world.CreateEntity();
      entity.Add(new PositionComponent());

      Assert.IsTrue(entity.Has<PositionComponent>());
    }

    [TestMethod]
    public void RemoveComponent_Test()
    {
      var entity = _world.CreateEntity();
      entity.Add(new PositionComponent());

      bool actual = entity.Remove<PositionComponent>();

      Assert.IsTrue(actual);
    }

    [TestMethod]
    public void RemoveUnknownComponent_Test()
    {
      var entity = _world.CreateEntity();
      entity.Add(new PositionComponent());

      bool actual = entity.Remove<VelocityComponent>();

      Assert.IsFalse(actual);
    }

  }
}
