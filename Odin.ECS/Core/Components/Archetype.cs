﻿using System;
using System.Collections.Generic;

namespace Odin.ECS.Core.Components
{
  public class Archetype
  {
    private readonly IEnumerable<Func<IComponent>> _creators;
    private readonly IEnumerable<Type> _types;

    internal Archetype(IEnumerable<Func<IComponent>> creators)
    {
      _creators = creators;
    }

    internal Archetype(IEnumerable<Type> types)
    {
      _types = types;
    }

    internal IEnumerable<IComponent> Construct()
    {
      if (_types == null && _creators == null)
      {
        throw new InvalidOperationException("Cannot create an empty archetype.");
      }

      if (_types != null)
      {
        foreach (Type type in _types)
        {
          yield return Activator.CreateInstance(type) as IComponent;
        }
      }

      if (_creators != null)
      {
        foreach (Func<IComponent> creator in _creators)
        {
          yield return creator.Invoke();
        }
      }
    }

  }
}
