﻿using Odin.ECS.Core.Entities;
using Odin.ECS.Util;
using System;
using System.Collections.Generic;

namespace Odin.ECS.Core.Components
{
  public enum MatchType
  {
    Any,
    All,
    None
  }

  public sealed class ComponentGroup : IEquatable<ComponentGroup>
  {
    private static readonly Dictionary<Bits, ComponentGroup> _anyGroups = new Dictionary<Bits, ComponentGroup>();
    private static readonly Dictionary<Bits, ComponentGroup> _allGroups = new Dictionary<Bits, ComponentGroup>();
    private static readonly Dictionary<Bits, ComponentGroup> _noneGroups = new Dictionary<Bits, ComponentGroup>();

    private readonly Type[] _components;

    public MatchType MatchType { get; }
    internal Bits Signature { get; }

    private ComponentGroup(MatchType matchType, params Type[] components)
    {
      MatchType = matchType;
      _components = components;
      Signature = ComponentType.GetBitsFor(components);
    }

    public bool Matches(Entity entity)
    {
      if (entity == null)
      {
        throw new ArgumentNullException(nameof(entity), "Cannot match a null entity.");
      }

      switch (MatchType)
      {
        case MatchType.Any:
          return entity.Signature.Insersects(Signature);
        case MatchType.All:
          return entity.Signature.Equals(Signature);
        case MatchType.None:
          return !entity.Signature.Insersects(Signature);
        default:
          throw new InvalidOperationException($"Unknown component group match type '{MatchType}'.");
      }
    }

    private static Dictionary<Bits, ComponentGroup> GetGroupDictionary(MatchType matchType)
    {
      switch (matchType)
      {
        case MatchType.Any:
          return _anyGroups;
        case MatchType.All:
          return _allGroups;
        case MatchType.None:
          return _noneGroups;
        default:
          throw new InvalidOperationException($"Unknown component group match type '{matchType}'.");
      }
    }

    internal static ComponentGroup GetGroup(Bits signature)
    {
      // TODO: This ain't great.
      foreach (MatchType matchType in Enum.GetValues(typeof(MatchType)))
      {
        ComponentGroup group = GetGroup(matchType, signature);
        if (group != null)
        {
          return group;
        }
      }

      return null;
    }

    internal static ComponentGroup GetGroup(MatchType matchType, Bits signature)
    {
      if (GetGroupDictionary(matchType).TryGetValue(signature, out ComponentGroup group))
      {
        return group;
      }
      return null;
    }

    internal static void AddGroup(ComponentGroup group)
    {
      GetGroupDictionary(group.MatchType).Add(group.Signature, group);
    }

    internal static void RemoveGroup(ComponentGroup group)
    {
      RemoveGroup(group.MatchType, group.Signature);
    }

    internal static void RemoveGroup(MatchType matchType, Bits signature)
    {
      GetGroupDictionary(matchType).Remove(signature);
    }

    public static ComponentGroup Any(params Type[] components)
    {
      ValidateComponents(components);
      return Create(MatchType.Any, components);
    }

    public static ComponentGroup All(params Type[] components)
    {
      ValidateComponents(components);
      return Create(MatchType.All, components);
    }

    public static ComponentGroup None(params Type[] components)
    {
      ValidateComponents(components);
      return Create(MatchType.None, components);
    }

    private static void ValidateComponents(params Type[] components)
    {
      if (components.Length == 0)
      {
        throw new ArgumentNullException(nameof(components), "Cannot create an empty component group.");
      }
    }

    private static ComponentGroup Create(MatchType matchType, params Type[] components)
    {
      var group = new ComponentGroup(matchType, components);
      AddGroup(group);
      return group;
    }

    public override bool Equals(object obj)
    {
      return Equals(obj as ComponentGroup);
    }

    public override int GetHashCode()
    {
      return Signature.GetHashCode();
    }

    public bool Equals(ComponentGroup other)
    {
      return other?.Signature.Equals(Signature) ?? false;
    }
  }
}
