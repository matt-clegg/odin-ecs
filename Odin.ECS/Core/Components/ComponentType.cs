﻿using Odin.ECS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Odin.ECS.Core.Components
{
  internal sealed class ComponentType
  {
    private static int _nextTypeId;
    private static readonly Dictionary<Type, ComponentType> _types = new Dictionary<Type, ComponentType>();

    public int Id { get; }

    private ComponentType(int id)
    {
      Id = id;
    }

    public static ComponentType GetType<T>()
    {
      return GetType(typeof(T));
    }

    public static ComponentType GetType(Type type)
    {
      if (!_types.TryGetValue(type, out ComponentType componentType))
      {
        componentType = new ComponentType(_nextTypeId++);
        _types.Add(type, componentType);
      }
      return componentType;
    }

    public static int GetIdFor(Type type)
    {
      if (_types.TryGetValue(type, out ComponentType componentType))
      {
        return componentType.Id;
      }

      return -1;
    }

    public static Bits GetBitsFor(IEnumerable<IComponent> components)
    {
      return GetBitsFor(components.Select(c => c.GetType()));
    }

    public static Bits GetBitsFor(IEnumerable<Type> types)
    {
      var bits = new Bits();

      foreach (Type type in types)
      {
        bits.Set(GetType(type).Id);
      }

      return bits;
    }
  }
}
