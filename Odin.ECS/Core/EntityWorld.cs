﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using Odin.ECS.Core.Systems;
using Odin.ECS.Events;
using Odin.ECS.Requests;
using System;
using System.Collections.Generic;

namespace Odin.ECS.Core
{
  public class EntityWorld
  {
    private readonly SystemManager _systemManager = new SystemManager();
    private readonly EntityManager _entityManager;

    private readonly Queue<IRequest> _delayedRequests = new Queue<IRequest>();

    internal TinyMessengerHub EventBus { get; }

    internal bool IsUpdating { get; private set; }

    public EntityWorld()
    {
      _entityManager = new EntityManager(this);
      EventBus = new TinyMessengerHub();

      _systemManager.SystemAddedEvent += _entityManager.OnSystemAdded;
      _systemManager.SystemRemovedEvent += _entityManager.OnSystemRemoved;
    }

    public void Init()
    {
      _systemManager.Init();
    }

    public void Update()
    {
      IsUpdating = true;

      _systemManager.Update();
      _entityManager.Update();

      IsUpdating = false;

      ProcessDelayedRequests();
    }

    public void Draw()
    {
      _systemManager.Draw();
    }

    private void ProcessDelayedRequests()
    {
      while (_delayedRequests.Count > 0)
      {
        IRequest next = _delayedRequests.Dequeue();
        next.Process();
      }
    }

    public void Cleanup()
    {
      _systemManager.Cleanup();
      _systemManager.SystemAddedEvent -= _entityManager.OnSystemAdded;
      _systemManager.SystemRemovedEvent -= _entityManager.OnSystemRemoved;
    }

    public Entity CreateEntity(Archetype archetype = null)
    {
      return _entityManager.Create(archetype);
    }

    public bool RemoveEntity(Entity entity)
    {
      return _entityManager.Remove(entity);
    }

    public Archetype CreateArchetype(params Func<IComponent>[] creators)
    {
      if (creators.Length == 0)
      {
        throw new ArgumentException("Cannot create an empty archetype.", nameof(creators));
      }

      return new Archetype(creators);
    }

    public Archetype CreateArchetype(params Type[] types)
    {
      if (types.Length == 0)
      {
        throw new ArgumentException("Cannot create an empty archetype.", nameof(types));
      }

      foreach (Type type in types)
      {
        if (!(type is IComponent))
        {
          throw new InvalidOperationException("Cannot create archetype. Types must implement from IComponent.");
        }
      }

      return new Archetype(types);
    }

    public BaseSystem AddSystem(BaseSystem system)
    {
      _systemManager.Add(system);
      system.RegisterWorld(this);
      return system;
    }

    public bool RemoveSystem(BaseSystem system)
    {
      return _systemManager.Remove(system);
    }

    internal void MakeDelayedRequest(IRequest request)
    {
      _delayedRequests.Enqueue(request);
    }

    public IReadOnlyCollection<Entity> GetEntities(ComponentGroup group = null)
    {
      return _entityManager.GetEntities(group);
    }

  }
}
