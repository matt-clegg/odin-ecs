﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Systems;
using Odin.ECS.Events;
using Odin.ECS.Extensions;
using Odin.ECS.Requests;
using Odin.ECS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Odin.ECS.Core.Entities
{
  internal sealed class EntityManager
  {
    private readonly IReadOnlyCollection<Entity> _emptyEntityList = new List<Entity>();

    private int _nextEntityId;
    private bool _removeInactive;

    private readonly EntityWorld _world;

    private readonly Dictionary<ComponentGroup, List<Entity>> _groupsToEntities = new Dictionary<ComponentGroup, List<Entity>>();
    private readonly Dictionary<Bits, List<Entity>> _signaturesToEntities = new Dictionary<Bits, List<Entity>>();
    private readonly List<Entity> _entities = new List<Entity>();

    public EntityManager(EntityWorld world)
    {
      _world = world;
    }

    private static Func<List<Entity>> EntityListCreator => () => new List<Entity>();

    public void Update()
    {
      if (_removeInactive)
      {
        RemoveInactiveEntities();
        _removeInactive = false;
      }
    }

    private void RemoveInactiveEntities()
    {
      var toRemove = new List<Entity>();
      foreach (Entity entity in GetEntities().Where(e => !e.Active))
      {
        toRemove.Add(entity);
      }

      foreach (Entity entity in toRemove)
      {
        Remove(entity);
      }
    }

    public Entity Create(Archetype archetype = null)
    {
      var entity = new Entity(_nextEntityId++, _world);

      entity.ComponentAddedEvent += OnComponentChanged;
      entity.ComponentRemovedEvent += OnComponentChanged;
      entity.ActiveStateChangedEvent += OnEntityActiveStateChanged;

      if (archetype != null)
      {
        entity.Add(archetype.Construct());
      }

      if (_world.IsUpdating)
      {
        _world.MakeDelayedRequest(new CreateEntityRequest(this, entity));
        return entity;
      }

      return InternalCreate(entity);
    }

    internal Entity InternalCreate(Entity entity)
    {
      _entities.Add(entity);
      _signaturesToEntities.TryGetAddDefault(entity.Signature, EntityListCreator).Add(entity);
      return entity;
    }

    public bool Remove(Entity entity)
    {
      if (entity == null)
      {
        throw new ArgumentNullException(nameof(entity), "Cannot remove a null entity.");
      }

      entity.ComponentAddedEvent -= OnComponentChanged;
      entity.ComponentRemovedEvent -= OnComponentChanged;
      entity.ActiveStateChangedEvent -= OnEntityActiveStateChanged;

      _entities.Remove(entity);

      if (_signaturesToEntities.TryGetValue(entity.Signature, out List<Entity> signatureList))
      {
        signatureList.Remove(entity);

        if (signatureList.Count == 0)
        {
          _signaturesToEntities.Remove(entity.Signature);
        }
      }

      ComponentGroup group = ComponentGroup.GetGroup(entity.Signature);
      if (group != null && _groupsToEntities.TryGetValue(group, out List<Entity> entities))
      {
        entities.Remove(entity);
      }

      return false;
    }

    internal void OnSystemAdded(object sender, SystemChangedArgs args)
    {
      BaseSystem system = args.System;

      var entitiesList = new List<Entity>();
      _groupsToEntities.Add(system.Group, entitiesList);

      foreach (Entity entity in GetEntities())
      {
        if (system.Group.Matches(entity))
        {
          entitiesList.Add(entity);
        }
      }
    }

    internal void OnSystemRemoved(object sender, SystemChangedArgs args)
    {
      _groupsToEntities.Remove(args.System.Group);
    }

    private void OnComponentChanged(object sender, ComponentChangedArgs args)
    {
      if (_world.IsUpdating)
      {
        Console.WriteLine("World is updating, must delay component thing.");
      }

      Entity entity = (Entity)sender;
      Bits previousSignature = args.PreviousSignature;

      if (!_signaturesToEntities.TryGetValue(previousSignature, out List<Entity> entityList))
      {
        throw new InvalidOperationException($"Entity does not exist with signature '{previousSignature}'.");
      }

      entityList.Remove(entity);

      if (entityList.Count == 0)
      {
        _signaturesToEntities.Remove(previousSignature);
      }

      ComponentGroup previousGroup = ComponentGroup.GetGroup(previousSignature);
      ComponentGroup newGroup = ComponentGroup.GetGroup(entity.Signature);

      List<Entity> previousList = previousGroup != null ? _groupsToEntities[previousGroup] : null;
      List<Entity> newList = newGroup != null ? _groupsToEntities[newGroup] : null;

      previousList?.Remove(entity);
      newList?.Add(entity);

      Bits newSignature = entity.Signature;
      _signaturesToEntities.TryGetAddDefault(newSignature, EntityListCreator).Add(entity);
    }

    private void OnEntityActiveStateChanged(object sender, EventArgs args)
    {
      if (sender is Entity entity && !entity.Active)
      {
        _removeInactive = true;
      }
    }

    internal IReadOnlyCollection<Entity> GetEntities(ComponentGroup group = null)
    {
      if (group == null)
      {
        return _entities;
      }

      if (_groupsToEntities.TryGetValue(group, out List<Entity> entities))
      {
        return entities;
      }
      return _emptyEntityList;
    }

  }
}
