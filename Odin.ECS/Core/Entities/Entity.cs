﻿using Odin.ECS.Core.Components;
using Odin.ECS.Events;
using Odin.ECS.Requests;
using Odin.ECS.Util;
using System;
using System.Collections.Generic;

namespace Odin.ECS.Core.Entities
{
  public class Entity : IEquatable<Entity>
  {
    private readonly TypeDictionary<IComponent> _components = new TypeDictionary<IComponent>();

    internal event EventHandler<ComponentChangedArgs> ComponentAddedEvent;
    internal event EventHandler<ComponentChangedArgs> ComponentRemovedEvent;
    internal event EventHandler ActiveStateChangedEvent;

    private readonly EntityWorld _world;

    public int Id { get; }

    private bool _active = true;
    public bool Active
    {
      get => _active;
      set
      {
        _active = value;
        ActiveStateChangedEvent?.Invoke(this, EventArgs.Empty);
      }
    }

    internal Entity(int id, EntityWorld world)
    {
      Id = id;
      _world = world;
    }

    internal Bits Signature => ComponentType.GetBitsFor(_components.Values);

    public IEnumerable<IComponent> Add(IEnumerable<IComponent> components)
    {
      return AddMultiple(components);
    }

    public IEnumerable<IComponent> Add(params IComponent[] components)
    {
      return AddMultiple(components);
    }

    private IEnumerable<IComponent> AddMultiple(IEnumerable<IComponent> components)
    {
      if (_world.IsUpdating)
      {
        _world.MakeDelayedRequest(new ComponentAdditionRequest(this, components));
        return components;
      }

      foreach (IComponent component in components)
      {
        Add(component);
      }

      return components;
    }

    public IComponent Add(IComponent component)
    {
      if (_world.IsUpdating)
      {
        _world.MakeDelayedRequest(new ComponentAdditionRequest(this, component));
        return component;
      }

      Bits previousSignature = Signature;
      _components.Add(component);
      ComponentAddedEvent?.Invoke(this, new ComponentChangedArgs(previousSignature, component));
      return component;
    }

    public bool Remove<T>() where T : class, IComponent
    {
      if (!Has<T>())
      {
        return false;
      }

      if (_world.IsUpdating)
      {
        _world.MakeDelayedRequest(new ComponentRemovalRequest<T>(this));
        return true;
      }

      Bits previousSignature = Signature;
      IComponent removedComponent = Get<T>();

      ComponentRemovedEvent?.Invoke(this, new ComponentChangedArgs(previousSignature, removedComponent));
      return true;
    }

    public T Get<T>() where T : class, IComponent
    {
      if (_components.TryGetValue<T>(out IComponent component))
      {
        return (T)component;
      }

      throw new InvalidOperationException($"{ToString()} does not have the following component '{typeof(T)}'.");
    }

    public bool Has<T>() where T : class, IComponent
    {
      return _components.ContainsKey<T>();
    }

    public override bool Equals(object obj)
    {
      return Equals(obj as Entity);
    }

    public override int GetHashCode()
    {
      return Id.GetHashCode();
    }

    public override string ToString()
    {
      return $"Entity [{Id}]";
    }

    public bool Equals(Entity other)
    {
      return !(other is null) && Id == other.Id;
    }
  }
}
