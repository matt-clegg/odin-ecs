﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using Odin.ECS.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Odin.ECS.Core.Systems
{
  public abstract class BaseSystem
  {
    private readonly IList<TinyMessageSubscriptionToken> _subscriptionTokens = new List<TinyMessageSubscriptionToken>();

    public int? UpdatePriority { get; }
    public int? DrawPriority { get; }

    public ComponentGroup Group { get; }

    private EntityWorld _world;
    private TinyMessengerHub _eventBus;

    protected BaseSystem(ComponentGroup group, int? updatePriority = null, int? drawPriority = null)
    {
      Group = group;
      UpdatePriority = updatePriority;
      DrawPriority = drawPriority;
    }

    protected IReadOnlyCollection<Entity> GetEntities() => _world.GetEntities(Group);

    public abstract void Init();
    public abstract void Update();
    public abstract void Draw();

    public virtual void Cleanup()
    {
      // Optional override
    }

    internal void RegisterWorld(EntityWorld world)
    {
      _world = world;
      _eventBus = world.EventBus;
    }

    public TinyMessageSubscriptionToken SubscribeTo<T>(Action<T> deliveryAction) where T : class, ITinyMessage
    {
      TinyMessageSubscriptionToken token = _eventBus.Subscribe(deliveryAction);
      _subscriptionTokens.Add(token);
      return token;
    }

    public void UnsubscribeFrom<T>(TinyMessageSubscriptionToken subscriptionToken) where T : class, ITinyMessage
    {
      _eventBus.Unsubscribe<T>(subscriptionToken);
    }

    public void PublishEvent(ITinyMessage eventMessage)
    {
      _eventBus.Publish(eventMessage);
    }

    internal void DisposeSubscriptionTokens()
    {
      foreach (TinyMessageSubscriptionToken token in _subscriptionTokens)
      {
        token.Dispose();
      }
    }

  }
}
