﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using System.Linq;

namespace Odin.ECS.Core.Systems
{
  public abstract class ProcessorSystem : BaseSystem
  {
    protected ProcessorSystem(ComponentGroup group, int priority = 0) : base(group, priority)
    {
    }

    protected abstract void Process(Entity entity);

    public sealed override void Update()
    {
      foreach (Entity entity in GetEntities())
      {
        Process(entity);
      }
    }
  }
}
