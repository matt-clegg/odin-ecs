﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using System.Linq;

namespace Odin.ECS.Core.Systems
{
  public abstract class IntervalProcessingSystem : IntervalSystem
  {
    protected IntervalProcessingSystem(ComponentGroup group, int tickInterval, int priority = 0) : base(group, tickInterval, priority)
    {
    }

    protected abstract void Process(Entity entity);

    protected override void DoUpdate()
    {
      foreach (Entity entity in GetEntities())
      {
        Process(entity);
      }
    }
  }
}
