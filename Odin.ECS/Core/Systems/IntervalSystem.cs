﻿using Odin.ECS.Core.Components;

namespace Odin.ECS.Core.Systems
{
  public abstract class IntervalSystem : BaseSystem
  {
    private readonly int _tickInterval;
    private int _currentTick;

    protected IntervalSystem(ComponentGroup group, int tickInterval, int priority = 0) : base(group, priority)
    {
      _tickInterval = tickInterval;
    }

    protected abstract void DoUpdate();

    public sealed override void Update()
    {
      if (_currentTick++ > _tickInterval)
      {
        DoUpdate();
        _currentTick = 0;
      }
    }
  }
}
