﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Odin.ECS.Core.Systems
{
  public abstract class ParallelSystem : BaseSystem
  {
    private readonly int _entityCount;

    protected ParallelSystem(ComponentGroup group, int entityCount, int priority = 0) : base(group, priority)
    {
      _entityCount = entityCount;
    }

    protected abstract void Process(Entity entity);

    public sealed override void Update()
    {
      if (GetEntities().Count() >= _entityCount)
      {
        Parallel.ForEach(GetEntities(), Process);
      }
      else
      {
        foreach (Entity entity in GetEntities())
        {
          Process(entity);
        }
      }
    }
  }
}
