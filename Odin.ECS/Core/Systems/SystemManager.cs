﻿using Odin.ECS.Core.Components;
using Odin.ECS.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Odin.ECS.Core.Systems
{
  internal sealed class SystemManager
  {
    private readonly List<BaseSystem> _updateSystems = new List<BaseSystem>();
    private readonly List<BaseSystem> _drawSystems = new List<BaseSystem>();

    public event EventHandler<SystemChangedArgs> SystemAddedEvent;
    public event EventHandler<SystemChangedArgs> SystemRemovedEvent;

    public void Init()
    {
      foreach (BaseSystem system in _updateSystems)
      {
        system.Init();
      }

      foreach (BaseSystem system in _drawSystems)
      {
        system.Init();
      }
    }

    public void Update()
    {
      foreach (BaseSystem system in _updateSystems)
      {
        system.Update();
      }
    }

    public void Draw()
    {
      foreach ( BaseSystem system in _drawSystems )
      {
        system.Draw();
      }
    }

    internal void Cleanup()
    {
      foreach (BaseSystem system in _updateSystems)
      {
        system.DisposeSubscriptionTokens();
        system.Cleanup();
      }

      foreach (BaseSystem system in _drawSystems)
      {
        system.DisposeSubscriptionTokens();
        system.Cleanup();
      }
    }

    public void Add(BaseSystem system)
    {
      if (system == null)
      {
        throw new ArgumentNullException(nameof(system), "Cannot add a null system.");
      }

      if ( system.UpdatePriority != null ) {
        _updateSystems.Add(system);
        _updateSystems.Sort(CompareUpdateSystems);
      }

      if ( system.DrawPriority != null ) {
        _drawSystems.Add(system);
        _drawSystems.Sort(CompareDrawSystems);
      }

      if ( system.UpdatePriority == null && system.DrawPriority == null ) {
        _updateSystems.Add( system );
        _drawSystems.Add( system );
        _updateSystems.Sort(CompareUpdateSystems);
        _drawSystems.Sort(CompareDrawSystems);
      }

      SystemAddedEvent?.Invoke(this, new SystemChangedArgs(system));
    }

    public bool Remove(BaseSystem system)
    {
      if (system == null)
      {
        throw new ArgumentNullException(nameof(system), "Cannot remove a null system.");
      }

      if (_updateSystems.Remove(system) || _drawSystems.Remove( system ))
      {
        system.DisposeSubscriptionTokens();
        ComponentGroup.RemoveGroup(system.Group);
        SystemRemovedEvent?.Invoke(this, new SystemChangedArgs(system));
        return true;
      }
      return false;
    }

    private static int CompareUpdateSystems(BaseSystem a, BaseSystem b)
    {
      return (b.UpdatePriority ?? 0).CompareTo(a.UpdatePriority ?? 0);
    }

    private static int CompareDrawSystems(BaseSystem a, BaseSystem b)
    {
      return (b.DrawPriority ?? 0).CompareTo(a.DrawPriority ?? 0);
    }
  }
}
