﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using System;
using System.Linq;

namespace Odin.ECS.Core.Systems
{
  public abstract class PredicateSystem : BaseSystem
  {
    private readonly Predicate<Entity> _predicate;

    protected PredicateSystem(ComponentGroup group, Predicate<Entity> predicate, int priority = 0) : base(group, priority)
    {
      _predicate = predicate;
    }

    protected abstract void Process(Entity entity);

    public sealed override void Update()
    {
      foreach (Entity entity in GetEntities().Where(e => _predicate(e)))
      {
        Process(entity);
      }
    }
  }
}
