﻿using System;
using System.Text;

namespace Odin.ECS.Util
{
  internal class Bits : ICloneable
  {
    private long[] _bits = { 0 };

    public Bits() { }

    public Bits(int nbits)
    {
      CheckCapacity(ShiftRight(nbits, 6));
    }

    public int NumBits => _bits.Length << 6;

    public void Or(Bits other)
    {
      int commonWords = Math.Min(_bits.Length, other._bits.Length);
      for (int i = 0; commonWords > i; i++)
      {
        _bits[i] |= other._bits[i];
      }

      if (commonWords < other._bits.Length)
      {
        CheckCapacity(other._bits.Length);
        for (int i = commonWords, s = other._bits.Length; s > i; i++)
        {
          _bits[i] = other._bits[i];
        }
      }
    }

    public bool Get(int index)
    {
      int word = ShiftRight(index, 6);
      if (word >= _bits.Length) return false;
      return (_bits[word] & (1L << (index & 0x3F))) != 0L;
    }

    public void Set(int index)
    {
      int word = ShiftRight(index, 6);
      CheckCapacity(word);
      _bits[word] |= 1L << (index & 0x3F);
    }

    public void Clear(int index)
    {
      int word = ShiftRight(index, 6);
      if (word >= _bits.Length) return;
      _bits[word] &= ~(1L << (index & 0x3F));
    }

    public int Length()
    {
      long[] bits = _bits;
      for (int word = bits.Length - 1; word >= 0; --word)
      {
        long bitsAtWord = bits[word];
        if (bitsAtWord != 0)
        {
          for (int bit = 63; bit >= 0; --bit)
          {
            if ((bitsAtWord & (1L << (bit & 0x3F))) != 0L)
            {
              return (word << 6) + bit + 1;
            }
          }
        }
      }

      return 0;
    }

    public bool IsEmpty()
    {
      long[] bits = _bits;
      for (int i = 0; i < bits.Length; i++)
      {
        if (bits[i] != 0) return false;
      }
      return true;
    }

    public bool Insersects(Bits other)
    {
      long[] bits = _bits;
      long[] otherBits = other._bits;

      for (int i = Math.Min(bits.Length, otherBits.Length) - 1; i >= 0; i--)
      {
        if ((bits[i] & otherBits[i]) != 0) return true;
      }

      return false;
    }

    public bool ContainsAll(Bits other)
    {
      long[] bits = _bits;
      long[] otherBits = other._bits;
      int otherBitsLength = otherBits.Length;
      int bitsLength = bits.Length;

      for (int i = bitsLength; i < otherBitsLength; i++)
      {
        if (otherBits[i] != 0) return false;
      }

      for (int i = Math.Min(bitsLength, otherBitsLength) - 1; i >= 0; i--)
      {
        if ((bits[i] & otherBits[i]) != otherBits[i]) return false;
      }

      return true;
    }

    private void CheckCapacity(int len)
    {
      if (len >= _bits.Length)
      {
        long[] newBits = new long[len + 1];
        Array.Copy(_bits, 0, newBits, 0, _bits.Length);
        _bits = newBits;
      }
    }

    public override int GetHashCode()
    {
      int word = ShiftRight(Length(), 6);
      int hash = 0;
      for (int i = 0; word >= i; i++)
      {
        hash = 127 * hash + (int)(_bits[i] ^ ShiftRight(_bits[i], 32));
      }

      return hash;
    }

    public override bool Equals(object obj)
    {
      if (this == obj) return true;
      if (obj == null) return false;
      if (GetType() != obj.GetType()) return false;

      Bits other = obj as Bits;
      long[] otherBits = other._bits;

      int commonWords = Math.Min(_bits.Length, otherBits.Length);

      for (int i = 0; commonWords > i; i++)
      {
        if (_bits[i] != otherBits[i]) return false;
      }

      if (_bits.Length == otherBits.Length) return true;

      return Length() == other.Length();
    }

    private int ShiftRight(long value, int shift)
    {
      return (int)((uint)value >> shift);
    }

    public override string ToString()
    {
      var builder = new StringBuilder();
      builder.Append("[");
      for (int i = 0; i < _bits.Length; i++)
      {
        builder.Append(_bits[i]);
        if (i != _bits.Length - 1)
        {
          builder.Append(", ");
        }
      }

      builder.Append("]");
      return builder.ToString();
    }

    public object Clone()
    {
      return new Bits
      {
        _bits = _bits
      };
    }
  }
}
