﻿using System;
using System.Collections.Generic;

namespace Odin.ECS.Util
{
  public class TypeDictionary<T> : Dictionary<Type, T>
  {
    public T Get<K>()
    {
      return this[typeof(K)];
    }

    public void Add(T value)
    {
      Add(value.GetType(), value);
    }

    public bool Remove<K>()
    {
      return Remove(typeof(K));
    }

    public bool TryGetValue<K>(out T value)
    {
      return TryGetValue(typeof(K), out value);
    }

    public bool ContainsKey<K>()
    {
      return ContainsKey(typeof(K));
    }


  }
}
