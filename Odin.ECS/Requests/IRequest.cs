﻿namespace Odin.ECS.Requests
{
  internal interface IRequest
  {
    void Process();
  }
}
