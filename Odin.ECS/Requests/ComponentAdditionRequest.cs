﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using System.Collections.Generic;

namespace Odin.ECS.Requests
{
  internal class ComponentAdditionRequest : IRequest
  {
    private Entity _entity;
    private IEnumerable<IComponent> _components;

    public ComponentAdditionRequest(Entity entity, IEnumerable<IComponent> components)
    {
      SetFields(entity, components);
    }

    public ComponentAdditionRequest(Entity entity, params IComponent[] components)
    {
      SetFields(entity, components);
    }

    private void SetFields(Entity entity, IEnumerable<IComponent> components)
    {
      _entity = entity;
      _components = components;
    }

    public void Process()
    {
      _entity.Add(_components);
    }
  }
}
