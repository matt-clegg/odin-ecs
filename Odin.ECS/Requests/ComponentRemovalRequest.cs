﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;

namespace Odin.ECS.Requests
{
  internal class ComponentRemovalRequest<T> : IRequest where T : class, IComponent
  {
    private readonly Entity _entity;

    public ComponentRemovalRequest(Entity entity)
    {
      _entity = entity;
    }

    public void Process()
    {
      _entity.Remove<T>();
    }
  }
}
