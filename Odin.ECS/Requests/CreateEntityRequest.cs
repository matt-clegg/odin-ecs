﻿using Odin.ECS.Core.Entities;

namespace Odin.ECS.Requests
{
  internal class CreateEntityRequest : IRequest
  {
    private readonly EntityManager _entityManager;
    private readonly Entity _entity;

    public CreateEntityRequest(EntityManager entityManager, Entity entity)
    {
      _entityManager = entityManager;
      _entity = entity;
    }

    public void Process()
    {
      _entityManager.InternalCreate(_entity);
    }
  }
}
