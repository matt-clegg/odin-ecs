﻿using Odin.ECS.Core.Systems;
using System;

namespace Odin.ECS.Events
{
  internal class SystemChangedArgs : EventArgs
  {
    public BaseSystem System { get; }

    public SystemChangedArgs(BaseSystem system)
    {
      System = system;
    }
  }
}
