﻿using Odin.ECS.Core.Components;
using Odin.ECS.Util;
using System;

namespace Odin.ECS.Events
{
  internal class ComponentChangedArgs : EventArgs
  {
    public Bits PreviousSignature { get; }
    public IComponent Component { get; }

    public ComponentChangedArgs(Bits previousSignature, IComponent component)
    {
      PreviousSignature = previousSignature;
      Component = component;
    }
  }
}
