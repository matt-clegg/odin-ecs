﻿using System;
using System.Collections.Generic;

namespace Odin.ECS.Extensions
{
  public static class DictionaryExtensions
  {
    public static T TryGetAddDefault<K, T>(this Dictionary<K, T> dictionary, K key, Func<T> constructor)
    {
      if (!dictionary.TryGetValue(key, out T item))
      {
        item = constructor.Invoke();
        dictionary.Add(key, item);
      }

      return item;
    }
  }
}
