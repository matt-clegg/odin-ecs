﻿using Odin.ECS.Core;
using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using Odin.Samples.Components;
using Odin.Samples.Systems;
using System;
using System.Diagnostics;
using System.Threading;

namespace Odin.Samples
{
    class Program
    {
        static void Main(string[] args)
        {
            // ONLY HAVE ONE METHOD UNCOMMENTED AT A TIME.
            // There's some code that uses static methods and is not reset/cleared/separate between new World classes.

            //HighEntityCountExample();
            //PriorityTest();
            //MemoryTest();
            ParallelBenchmark();
            //WorkingExample();
            //CrashExample();

            Console.WriteLine("Done");
            Console.ReadKey();
        }

        private static void HighEntityCountExample()
        {
            var world = new EntityWorld();
            var random = new Random();

            const int entityCount = 100000;

            var watch = Stopwatch.StartNew();

            for (int i = 0; i < entityCount; i++)
            {
                Entity entity = world.CreateEntity();
                entity.Add(new PositionComponent(random.Next(-100, 100), random.Next(-100, 100)));
                entity.Add(new VelocityComponent((float)random.NextDouble(), (float)random.NextDouble()));
                entity.Add(new WanderComponent((float)(random.NextDouble() / 2)));
            }
            watch.Stop();
            Console.WriteLine(watch.Elapsed.TotalMilliseconds + "ms" + " (" + (watch.Elapsed.TotalMilliseconds / entityCount) + " ms each)");

            world.AddSystem(new WanderSystem());

            const int ticks = 10000;
            int tick = 0;

            while (tick++ < ticks)
            {
                world.Update();
                Thread.Sleep(16);
            }
        }

        private static void PriorityTest()
        {
            var world = new EntityWorld();

            const int ticks = 10;

            var group = ComponentGroup.All(typeof(PositionComponent));

            // Higher priority value = higher in order = process sooner.

            // Movement System will update last and render first.
            //world.AddSystem( new MovementSystem( group, 1, 2 ) );
            // Label System will update first and render last
            world.AddSystem(new LabelSystem(2, 1));

            for (int i = 0; i < ticks; i++)
            {
                world.Update();
                world.Draw();
                Console.WriteLine("-----");
            }
        }

        private static void MemoryTest()
        {
            var world = new EntityWorld();
            var random = new Random();

            const int entitiesToCreate = 1;

            for (int i = 0; i < entitiesToCreate; i++)
            {
                Entity entity = world.CreateEntity();
                entity.Add(new PositionComponent(random.Next(100), random.Next(100)));
                //if (random.NextDouble() < 0.5)
                //{
                //  entity.Add(new PositionComponent(random.Next(100), random.Next(100)));
                //}

                //if (random.NextDouble() < 0.5)
                //{
                //  entity.Add(new VelocityComponent((float) random.NextDouble(), (float) random.NextDouble()));
                //}
            }

            //world.AddSystem(new MovementSystem(ComponentGroup.All(typeof(PositionComponent), typeof(VelocityComponent))));
            //world.AddSystem(new RandomizerSystem(ComponentGroup.Any(typeof(PositionComponent)), random, world));
            world.AddSystem(new DeactivatorSystem());

            const int ticks = 2;

            for (int i = 0; i < ticks; i++)
            {
                Console.WriteLine(i + " - Entities: " + world.GetEntities().Count);
                world.Update();



                // Simulate 60fps
                Thread.Sleep(50);
            }
        }

        private static void ParallelBenchmark()
        {
            var world = new EntityWorld();
            var random = new Random();

            // Set the entitiesNeededToParallel to 1001 to use a single thread.
            // Set the entitiesNeededToParallel to 1000 to use multiple threads.
            const int entitiesToCreate = 1000;
            const int entitiesNeededToParallel = 1001;

            for (int i = 0; i < entitiesToCreate; i++)
            {
                Entity entity = world.CreateEntity();
                entity.Add(new PositionComponent(10, 10), new VelocityComponent((float)random.NextDouble(), (float)random.NextDouble()));
            }

            // Look in the MovementParallelSystem class for more info.
            world.AddSystem(new MovementParallelSystem(ComponentGroup.All(typeof(PositionComponent), typeof(VelocityComponent)), entitiesNeededToParallel));

            const int ticks = 1000;

            var watch = Stopwatch.StartNew();
            for (int i = 0; i < ticks; i++)
            {
                world.Update();
            }
            watch.Stop();

            // Running in release mode, outside of Visual Studio: 
            // Intel i7-7700 @ 3.6GHz
            // The parallel version takes 1.6 seconds.
            // The single thread version takes 6.8 seconds.

            Console.WriteLine("Total: " + watch.Elapsed.TotalMilliseconds + "ms Avg: " + (watch.Elapsed.TotalMilliseconds / ticks) + "ms");
        }

        private static void WorkingExample()
        {
            var world = new EntityWorld();

            // Create a component group that matches entities containing none of the following components.
            ComponentGroup noneGroup = ComponentGroup.None(typeof(PositionComponent), typeof(VelocityComponent));

            // Create a component group that matches entities containing the following components only.
            ComponentGroup allGroup = ComponentGroup.All(typeof(PositionComponent), typeof(VelocityComponent));

            // Create a component group that matches entities containing ANY of the following components.
            ComponentGroup anyGroup = ComponentGroup.Any(typeof(PositionComponent), typeof(VelocityComponent));

            // Use the anyGroup on a movement system
            // Usually, you won't define the ComponentGroup here. You would define it inside the MovementSystem class
            // in the base() call to the base class constractor. Like so:
            // public MovementSystem : base(ComponentGroup.Any(typeof(ComponentA), typeof(ComponentB)) {}
            var anySystem = world.AddSystem(new MovementSystem());

            // Create an entity with a position and velocity component
            Entity entity = world.CreateEntity();
            entity.Add(new PositionComponent(10, 10));
            entity.Add(new VelocityComponent(1f, 0.5f));

            // Not required, but it calls all registered systems Init() methods.
            world.Init();

            const int ticks = 10;

            for (int i = 0; i < ticks; i++)
            {
                world.Update();
            }

            // The Entity position should be x: 20 y: 15
            PositionComponent position = entity.Get<PositionComponent>();
            Console.WriteLine("x: " + position.X + " y: " + position.Y);
        }

        private static void CrashExample()
        {
            var world = new EntityWorld();

            // Create an entity with a position and velocity component.
            Entity entityA = world.CreateEntity();
            entityA.Add(new PositionComponent(10, 10));
            entityA.Add(new VelocityComponent(1, 0.5f));

            // Create an entity with just a position component.
            Entity entityB = world.CreateEntity();
            entityB.Add(new PositionComponent(10, 10));

            // Create a movement system that accepts entities that have EITHER a position or velocity component.
            ComponentGroup anyGroup = ComponentGroup.Any(typeof(PositionComponent), typeof(VelocityComponent));
            var anySystem = world.AddSystem(new MovementSystem());

            try
            {
                // This will crash!
                world.Update();
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine("World Update Exception: " + ex.Message);
            }

            // This crashes because the Movement System does work using the position and velocity components. 
            // The system was set to an ANY group, so any entities that had either one or both of those components were
            // used in the Movement System update loop. When the system tries to get a component on an entity that 
            // doesn't have one, it will crash. 

            // In this case, you should use the ALL component group.
            // Be careful what Component Group match types you use.
        }
    }
}
