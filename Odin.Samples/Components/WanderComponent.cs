﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Odin.ECS.Core.Components;

namespace Odin.Samples.Components
{
  public class WanderComponent : IComponent
  {
    public float Frequency
    {
      get;
      set;
    }

    public WanderComponent(float frequency)
    {
      Frequency = frequency;
    }
  }
}
