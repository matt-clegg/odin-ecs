﻿using Odin.ECS.Core.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odin.Samples.Components
{
  public class LabelComponent : IComponent
  {
    public string Text { get; set; }

    public LabelComponent(string text)
    {
      Text = text;
    }
  }
}
