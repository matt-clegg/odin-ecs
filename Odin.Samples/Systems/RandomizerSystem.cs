﻿using Odin.ECS.Core;
using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using Odin.ECS.Core.Systems;
using Odin.Samples.Components;
using System;

namespace Odin.Samples.Systems
{
  public class RandomizerSystem : BaseSystem
  {
    private readonly Random _random;
    private readonly EntityWorld _world;

    public RandomizerSystem(ComponentGroup group, Random random, EntityWorld world, int priority = 0) : base(group, priority)
    {
      _random = random;
      _world = world;
    }

    public override void Init()
    {

    }

    public override void Update()
    {
      int componentsAdded = 0;
      int componentsRemoved = 0;
      int entititesAdded = 0;
      int entititesRemoved = 0;
      foreach (Entity entity in GetEntities())
      {
        if (_random.NextDouble() < 0.05)
        {
          if (!entity.Has<PositionComponent>())
          {
            entity.Add(new PositionComponent(_random.Next(100), _random.Next(100)));
            componentsAdded++;
          }
          if (!entity.Has<LabelComponent>())
          {
            entity.Add(new LabelComponent("hello"));
            componentsAdded++;
          }
          if (!entity.Has<VelocityComponent>())
          {
            entity.Add(new VelocityComponent((float)_random.NextDouble(), (float)_random.NextDouble()));
            componentsAdded++;
          }
        }

        if (_random.NextDouble() < 0.05)
        {
          if (entity.Has<LabelComponent>())
          {
            entity.Remove<LabelComponent>();
            componentsRemoved++;
          }
          if (entity.Has<PositionComponent>())
          {
            entity.Remove<PositionComponent>();
            componentsRemoved++;
          }
          if (entity.Has<VelocityComponent>())
          {
            entity.Remove<VelocityComponent>();
            componentsRemoved++;
          }
        }


        if (_random.NextDouble() < 0.05)
        {
          entity.Active = false;
          entititesRemoved++;
        }
      }
      if(_random.NextDouble() < 0.05)
      {
        Entity newEntity = _world.CreateEntity();
        newEntity.Add(new LabelComponent("world"));
        entititesAdded++;
      }

      if(_random.NextDouble() < 0.02)
      {
        for(int i = 0; i < 25; i++)
        {
          Entity newEntity = _world.CreateEntity();
          newEntity.Add(new LabelComponent("world"));
          entititesAdded++;
        }
      }

      Console.WriteLine("Components (-" + componentsRemoved + ") (+" + componentsAdded + " ) - Entities (-" + entititesRemoved + ") (+" + entititesAdded + ")" );
    }

    public override void Draw()
    {
      throw new NotImplementedException();
    }
  }
}
