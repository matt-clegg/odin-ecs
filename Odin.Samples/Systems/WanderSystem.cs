﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using Odin.ECS.Core.Systems;
using Odin.Samples.Components;

namespace Odin.Samples.Systems
{
  public class WanderSystem : ProcessorSystem
  {
    private readonly Random _random;

    public WanderSystem( ) : base( ComponentGroup.Any( typeof(VelocityComponent), typeof(WanderComponent) ))
    {
      _random = new Random();
    }

    public override void Init()
    {
    }

    public override void Draw()
    {
    }

    protected override void Process( Entity entity )
    {
      if ( _random.NextDouble() <= entity.Get<WanderComponent>().Frequency ) {
        VelocityComponent vel = entity.Get<VelocityComponent>();

        vel.X = (float) _random.NextDouble();
        vel.Y = (float) _random.NextDouble();
      }
    }
  }
}
