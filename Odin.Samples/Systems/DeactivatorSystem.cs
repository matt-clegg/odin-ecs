﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using Odin.ECS.Core.Systems;
using Odin.Samples.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odin.Samples.Systems
{
  public class DeactivatorSystem : ProcessorSystem
  {
    public DeactivatorSystem(int priority = 0) : base(ComponentGroup.Any(typeof(PositionComponent)), priority)
    {
    }

    public override void Init()
    {
      throw new NotImplementedException();
    }

    public override void Draw()
    {
      throw new NotImplementedException();
    }

    protected override void Process(Entity entity)
    {
      entity.Active = false;
    }
  }
}
