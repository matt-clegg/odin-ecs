﻿using System;
using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using Odin.ECS.Core.Systems;
using Odin.Samples.Components;

namespace Odin.Samples.Systems
{
  public class MovementSystem : ProcessorSystem
  {

    public MovementSystem() 
      : base(ComponentGroup.All( typeof(PositionComponent), typeof(VelocityComponent) ))
    {
    }

    public override void Init()
    {

    }

    protected override void Process( Entity entity )
    {
      PositionComponent position = entity.Get<PositionComponent>();
      VelocityComponent velocity = entity.Get<VelocityComponent>();

      position.X += velocity.X;
      position.Y += velocity.Y;
    }

    public override void Draw()
    {

    }

  }
}
