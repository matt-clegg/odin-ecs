﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Systems;
using Odin.Samples.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odin.Samples.Systems
{
  public class LabelSystem : BaseSystem
  {
    public LabelSystem(int? updatePriority = null, int? drawPriority = null) 
      : base(ComponentGroup.All(typeof(LabelComponent)), updatePriority, drawPriority)
    {
    }

    public override void Init()
    {

    }

    public override void Update()
    {
      Console.WriteLine( "Updating label system" );
    }

    public override void Draw()
    {
      Console.WriteLine("Drawing label system");
    }
  }
}
