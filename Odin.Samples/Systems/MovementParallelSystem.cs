﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Entities;
using Odin.ECS.Core.Systems;
using Odin.Samples.Components;
using System.Threading;

namespace Odin.Samples.Systems
{
  public class MovementParallelSystem : ParallelSystem
  {
    public MovementParallelSystem(ComponentGroup group, int entityCount, int priority = 0) : base(group, entityCount, priority)
    {
    }

    public override void Init()
    {

    }

    public override void Draw()
    {
      throw new System.NotImplementedException();
    }

    // Check the base ParallelSystem class to see the implementation.
    // Only use the Parallel.ForEach method if the number of entities this system processes is 
    // greater than a certain threshold. This is up to the user to decide, entirely depends on the
    // time the operation will take and the number of entities.

    // If you set the threshold too low, the overhead of using a Parallel forloop is much higher than
    // using a single thread to process the operation.

    protected override void Process(Entity entity)
    {
      // This represents an "expensive" operation to perform on an entity.
      // If the operation takes a long time, it's probably worth parallelizing it.
      for(int i = 0; i < 100; i++)
      {
        // Note you wouldn't need to get the components multiple times, I left the Get's inside the loop to increase the processing time.
        PositionComponent position = entity.Get<PositionComponent>();
        VelocityComponent velocity = entity.Get<VelocityComponent>();

        position.X += velocity.X;
        position.Y += velocity.Y;
      }
    }
  }
}
