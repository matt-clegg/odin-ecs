﻿using Odin.ECS.Core.Components;
using Odin.ECS.Core.Systems;
using Odin.Samples.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odin.Samples.Systems
{
  public class ExcludeSystem : BaseSystem
  {
    public ExcludeSystem() : base(ComponentGroup.None(typeof(PositionComponent)))
    {
    }

    public override void Init()
    {

    }

    public override void Update()
    {

    }

    public override void Draw()
    {
      
    }
  }
}
