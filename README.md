## Odin ECS
An [Entity Component System](https://en.wikipedia.org/wiki/Entity%E2%80%93component%E2%80%93system) written in C# that *tries* to be performant and easy to use.

---
*Disclaimer: This is not complete, may have bugs, severely lacks comments and has not been fully tested. I decided to upload the project to Gitlab for other people to look at and cretique. Feel free to use this ECS in any way or make contributions.* 

## Basic Usage
*(Take a look at Odin.Samples for more examples)*

#### Entities / Components
To begin, create an instance of the `EnityWorld` class to begin creating entities.

To create an entity, do the following:

    var world = new EntityWorld();
    Entity entity = world.CreateEntity();

To add a component to an entity:

    entity.Add(new PositionComponent());

Or,  you can add multiple components at the same time:

    // You can use any instance of IEnumerable, like a List
    IEnumerable<IComponent> list = new List<IComponent>() {
        new PositionComponent(),
        new VelocityComponent()
    };

    entity.Add(list);
    
    // Or an array.        
    IEnumerable<IComponent> array = new IComponent[] {
        new PositionComponent(),
        new VelocityComponent()
    };

    // Note: This would break as you are adding the same component types twice.
    entity.Add(array);
	
To get a component from an entity:

    IComponent pos = entity.Get<PositionCompoent>();

To check if an entity has a component:

    bool hasPos = entity.Has<PositionComponent>();

To remove components from an entity:

    bool success = entity.Remove<PositionComponent>();

Components must implement the interface `IComponent`:

    public class PositionComponent : IComponent {
        public float X { get; set; }
        public float Y { get; set; }
    
        public PositionComponent(float x, float y) {
            X = x;
            Y = y;
        }
    }
Components do not need an empty constructor.


### Systems
This is a basic example of a system. It loops over each entity that has a position and velocity component and performs some process on them.

      public class MovementSystem : ProcessorSystem
      {

        public MovementSystem() : base(ComponentGroup.All( typeof(PositionComponent), typeof(VelocityComponent) ))
        {
        }

        public override void Init()
        {
        }

        protected override void Process( Entity entity )
        {
            PositionComponent position = entity.Get<PositionComponent>();
            VelocityComponent velocity = entity.Get<VelocityComponent>();

            position.X += velocity.X;
            position.Y += velocity.Y;
        }

        public override void Draw()
        {
        }
     } 

#### Processor System
This is the most basic system to extend from. There is a simple process method that is called for each entity that matches the components specified in the constructor.

